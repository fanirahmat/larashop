<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Barang;
use App\Models\Customer;

class Transaksi extends Model
{
    use HasFactory;

    protected $table = "transaksis";
    protected $fillable = [
        'barang_id',
        'customer_id',
        'amount',
        'status',
    ];

    public function barangs()
    {
        return $this->belongsTo(Barang::class, 'barang_id', 'id');
    }

    public function customers()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
