<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Staff extends Authenticatable
{
    use HasFactory;

    protected $guard = "staff";

    protected $fillable = [
        'name',
        'gender',
        'username',
        'email',
        'password',
        'retype_password',
    ];

    protected $hidden = [
        'password',
        'retype_password',
        'remember_token',
    ];

}
