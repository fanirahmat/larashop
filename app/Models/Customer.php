<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Customer extends Authenticatable
{
    use HasFactory;

    protected $guard = "customer";

    protected $fillable = [
        'name',
        'gender',
        'address',
        'id_card_image',
        'dob',
        'username',
        'email',
        'password',
        'retype_password',
    ];

    protected $hidden = [
        'password',
        'retype_password',
        'remember_token',
    ];
}
