<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
     public function showCustomer()
    {
        return view('customer.customer')->with([
            'customer' => Customer::all(),
            'title' => 'Data Customer'
        ]);
    }

    public function showCustomerByID($id) {
        return view('customer.create-customer-form')->with([
            'customer' => Customer::find($id)
        ]);
    }

    public function showFormCreateCustomer()
    {
        return view('customer.create-customer-form');
    }

    public function createCustomer(Request $request) {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string'],
            'address' => ['required', 'string', 'max:255'],
            'id_card_image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'dob' => ['required', 'date'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            //'retype_password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $imageName = $request->id_card_image->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->id_card_image));

        if ($isImageUploaded) {
            $customer = Customer::create([
                        'name' => $request['name'],
                        'gender' => $request['gender'],
                        'address' => $request['address'],
                        'id_card_image' => $imagePath,
                        'dob' => $request['dob'],
                        'username' => $request['username'],
                        'email' => $request['email'],
                        'password' => Hash::make($request['password']),
                        'retype_password' => Hash::make($request['password']),
                    ]);
        }

        if (Auth::guard('admin')->check()) {
            return to_route('admin-customer-view');
        } else if (Auth::guard('staff')->check()) {
            return to_route('staff-customer-view');
        }

    }

    public function editCustomer(Request $request, $id) {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string'],
            'address' => ['required', 'string', 'max:255'],
            'id_card_image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'dob' => ['required', 'date'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            //'retype_password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $imageName = $request->id_card_image->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->id_card_image));

        if ($isImageUploaded) {
            $customer = Customer::find($id);
            $customer->name = $request->name;
            $customer->gender = $request->gender;
            $customer->address = $request->address;
            $customer->id_card_image = $imagePath;
            $customer->dob = $request->dob;
            $customer->username = $request->username;
            $customer->email = $request->email;
            $customer->password = Hash::make($request->password);
            $customer->retype_password = Hash::make($request->password);
            $customer->save();
        }
        
        if (Auth::guard('admin')->check()) {
            return to_route('admin-customer-view');
        } else if (Auth::guard('staff')->check()) {
            return to_route('staff-customer-view');
        }

    }

    public function deleteCustomer($id) {
        $customer = Customer::find($id);
        $customer->delete();
        return back();
    }
}
