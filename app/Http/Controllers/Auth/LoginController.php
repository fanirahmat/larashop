<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Staff;
use App\Models\Customer;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('guest:admin')->except('logout');
        // $this->middleware('guest:staff')->except('logout');
        // $this->middleware('guest:customer')->except('logout');
    }

    // Admin
    public function showAdminLoginForm()
    {
        return view('auth.login', [
            'route_link_register' => route('admin.register-view'),
            'route_post' => route('admin.login'), 
            'title'=>'Admin'
        ]);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (\Auth::guard('admin')->attempt($request->only(['email','password']), $request->get('remember'))){
            return redirect()->intended('/admin/dashboard');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    // Staff
    public function showStaffLoginForm()
    {
        return view('auth.login', [
            'route_link_register' => route('staff.register-view'), 
            'route_post' => route('staff.login'), 
            'title'=>'Staff'
        ]);
    }

    public function staffLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (\Auth::guard('staff')->attempt($request->only(['email','password']), $request->get('remember'))){
            return redirect()->intended('/staff/dashboard');
        }

        return back()->withInput($request->only('email', 'remember'));
    }

    // Customer
    public function showCustomerLoginForm()
    {
        return view('auth.login', [
            'route_link_register' => route('customer.register-view'), 
            'route_post' => route('customer.login'), 
            'title'=>'Customer'
        ]);
    }

    public function customerLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (\Auth::guard('customer')->attempt($request->only(['email','password']), $request->get('remember'))){
            return redirect()->intended('/customer/dashboard');
        }

        return back()->withInput($request->only('email', 'remember'));
    }
}
