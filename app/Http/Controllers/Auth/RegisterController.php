<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Staff;
use App\Models\Customer;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        // $this->middleware('guest:admin');
        // $this->middleware('guest:staff');
        // $this->middleware('guest:customer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    // Admin
    public function showAdminRegisterForm()
    {
        return view('auth.register', [
            'route_link_login' => route('admin.login-view'), 
            'route_post' => route('admin.register-view'), 
            'title'=>'Admin'
        ]);
    }

    protected function createAdmin(Request $request)
    {
        $this->validator($request->all())->validate();
        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        //return redirect()->intended('admin.login-view');
        return to_route('admin.login-view');
    }

    // Staff
    public function showStaffRegisterForm()
    {
        return view('auth.register-staff', [
            'route_link_login' => route('staff.login-view'), 
            'route_post' => route('staff.register'), 
            'title'=>'Staff']
        );
    }

    protected function createStaff(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $staff = Staff::create([
            'name' => $request['name'],
            'gender' => $request['gender'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'retype_password' => Hash::make($request['password']),
        ]);
        // return redirect()->intended('staff.login-view');
        return to_route('staff.login-view');
    }

    // Customer
    public function showCustomerRegisterForm()
    {
        return view('auth.register-customer', [
            'route_link_login' => route('customer.login-view'), 
            'route_post' => route('customer.register'), 
            'title'=>'Customer'
        ]);
    }

    protected function createCustomer(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string'],
            'address' => ['required', 'string', 'max:255'],
            'id_card_image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'dob' => ['required', 'date'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            //'retype_password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $imageName = $request->id_card_image->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->id_card_image));

        if ($isImageUploaded) {
            $customer = Customer::create([
                        'name' => $request['name'],
                        'gender' => $request['gender'],
                        'address' => $request['address'],
                        'id_card_image' => $imagePath,
                        'dob' => $request['dob'],
                        'username' => $request['username'],
                        'email' => $request['email'],
                        'password' => Hash::make($request['password']),
                        'retype_password' => Hash::make($request['password']),
                    ]);
        }
        
        return to_route('customer.login-view');
        // return redirect()->intended('customer.login-view');
    }
}
