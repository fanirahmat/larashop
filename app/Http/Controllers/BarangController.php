<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Laporan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{

    public function showBarang()
    {
        return view('barang.barang')->with([
                'barang' => Barang::all(),
                'title' => 'Data Barang'
            ]);
    }

    public function showBarangByID($id) {
        return view('barang.create-barang-form')->with([
            'barang' => Barang::find($id)
        ]);
    }

    public function showFormCreateBarang()
    {
        return view('barang.create-barang-form');
    }

    public function createBarang(Request $request) {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'stock' => ['required', 'integer'],
            'selling_price' => ['required', 'integer'],
            'purchase_price' => ['required', 'integer'],
            'image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ]);

        $imageName = $request->image->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->image));

        if ($isImageUploaded) {

            $barang_code = "";
            $isBarangCreated = Barang::create([
                'name' => $request['name'],
                'desc' => $request['desc'],
                'type' => $request['type'],
                'stock' => $request['stock'],
                'selling_price' => $request['selling_price'],
                'purchase_price' => $request['purchase_price'],
                'image' => $imagePath,
            ]);

            if ($isBarangCreated) {
                Laporan::create([
                    'barang_id' => $isBarangCreated->id,
                    'amount_sold' => 0,
                    'gross_profit' => 0,
                    'net_profit' => 0,
                ]);
            }
        }

        if (Auth::guard('admin')->check()) {
            return to_route('admin-barang-view');
        } else if (Auth::guard('staff')->check()) {
            return to_route('staff-barang-view');
        }

    }

    public function editBarang(Request $request, $id) {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'stock' => ['required', 'integer'],
            'selling_price' => ['required', 'integer'],
            'purchase_price' => ['required', 'integer'],
            'image' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ]);

        $imageName = $request->image->getClientOriginalName();  
        $imagePath = 'images/'. time() . '.'.$imageName;
        $isImageUploaded = Storage::disk('public')->put($imagePath, file_get_contents($request->image));

        if ($isImageUploaded) {
            $barang = Barang::find($id);
            $barang->name = $request->name;
            $barang->desc = $request->desc;
            $barang->type = $request->type;
            $barang->stock = $request->stock;
            $barang->selling_price = $request->selling_price;
            $barang->purchase_price = $request->purchase_price;
            $barang->image = $imagePath;
            $barang->save();
        }

        if (Auth::guard('admin')->check()) {
            return to_route('admin-barang-view');
        } else if (Auth::guard('staff')->check()) {
            return to_route('staff-barang-view');
        }
    }

    public function deleteBarang($id) {
        $barang = Barang::find($id);
        $imagePath = "public/".$barang->image; 
        if (Storage::exists($imagePath)) {
            Storage::delete($imagePath);
        }
        $barang->delete();

        return back()->with('success','Video has been successfully deleted.');
    }
}
