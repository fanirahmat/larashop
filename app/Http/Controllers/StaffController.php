<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Staff;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{

    public function showStaff()
    {
        return view('staff.staff')->with([
            'staff' => Staff::all(),
            'title' => 'Data Staff'
        ]);
    }

    public function showStaffByID($id) {
        return view('staff.create-staff-form')->with([
            'staff' => Staff::find($id)
        ]);
    }

    public function showFormCreateStaff()
    {
        return view('staff.create-staff-form');
    }

    public function createStaff(Request $request) {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $staff = Staff::create([
            'name' => $request['name'],
            'gender' => $request['gender'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'retype_password' => Hash::make($request['password']),
        ]);

        return to_route('admin-staff-view');

    }

    public function editStaff(Request $request, $id) {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $staff = Staff::find($id);
        $staff->name = $request->name;
        $staff->gender = $request->gender;
        $staff->username = $request->username;
        $staff->email = $request->email;
        $staff->password = Hash::make($request->password);
        $staff->retype_password = Hash::make($request->password);
        $staff->save();

        return to_route('admin-staff-view');
    }

    public function deleteStaff($id) {
        $barang = Staff::find($id);
        $barang->delete();
        return back();
    }
}
