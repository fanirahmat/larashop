<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Laporan;

class LaporanController extends Controller
{
    public function showLaporan() {
        return view('laporan')->with([
            'laporan' => Laporan::all(),
            'title' => 'Data Laporan Penjualan'
        ]);
    }
}
 