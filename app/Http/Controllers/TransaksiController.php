<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Customer;
use App\Models\Transaksi;
use App\Models\Laporan;

class TransaksiController extends Controller
{
    public function showTransaksi()
    {
        if (Auth::guard('customer')->check()) {
            $id = Auth::user()->id;
            $transaksi = Transaksi::where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        } else {
            $transaksi = Transaksi::orderBy('created_at', 'DESC')->get();
        }

        return view('transaksi.transaksi')->with([
            'transaksi' => $transaksi,
            'title' => 'Data Transaction'
        ]);
    }

    public function createTransaksi($barang_id) {
        $user_id = Auth::user()->id;
        $amount = 1;

        Transaksi::create([
            'amount' => $amount,
            'barang_id' => $barang_id,
            'customer_id' => $user_id,
            'status' => 'pending'
        ]);
        return to_route('customer-transaksi-view');
    }

    public function confirmTransaksi($id) {
        $transaksi = Transaksi::find($id);

        // update status transaksi
        $transaksi->update([
            'status' => 'accepted'
        ]);

        // kurangi stock barang
        $barang_id = $transaksi->barang_id;
        $barang = Barang::find($barang_id);
        $stock = $barang->stock;

        $total_stock = $stock - $transaksi->amount;
        $barang->update([
            'stock' => $total_stock 
        ]);
 

        // Tambah Jumlah Barang Terjual
        $laporan = Laporan::where('barang_id', $barang_id)->first();
        $amount_sold = $laporan->amount_sold + $transaksi->amount;
        $gross_profit = $amount_sold * $barang->selling_price;
        $net_profit = $amount_sold * ($barang->selling_price - $barang->purchase_price);
        $laporan->update([
            'amount_sold' => $amount_sold,
            'gross_profit' => $gross_profit,
            'net_profit' => $net_profit,
        ]);

        return back();
    }


}
