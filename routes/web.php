<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\LaporanController;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

// Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Admin
Route::get('/admin',[LoginController::class,'showAdminLoginForm'])->name('admin.login-view');
Route::post('/admin',[LoginController::class,'adminLogin'])->name('admin.login');

Route::get('/admin/register',[RegisterController::class,'showAdminRegisterForm'])->name('admin.register-view');
Route::post('/admin/register',[RegisterController::class,'createAdmin'])->name('admin.register');


// Staff
Route::get('/staff',[LoginController::class,'showStaffLoginForm'])->name('staff.login-view');
Route::post('/staff',[LoginController::class,'staffLogin'])->name('staff.login');

Route::get('/staff/register',[RegisterController::class,'showStaffRegisterForm'])->name('staff.register-view');
Route::post('/staff/register',[RegisterController::class,'createStaff'])->name('staff.register');


// Customer
Route::get('/customer',[LoginController::class,'showCustomerLoginForm'])->name('customer.login-view');
Route::post('/customer',[LoginController::class,'customerLogin'])->name('customer.login');

Route::get('/customer/register',[RegisterController::class,'showCustomerRegisterForm'])->name('customer.register-view');
Route::post('/customer/register',[RegisterController::class,'createCustomer'])->name('customer.register');


Route::middleware(['auth:admin'])->group(function () {
    Route::get('admin/dashboard',function() { 
        return view('dashboard');
    });

    // Barang
    Route::get('admin/barang',[BarangController::class,'showBarang'])->name('admin-barang-view');
    Route::get('admin/barang/{id}',[BarangController::class,'showBarangByID'])->name('admin-barang-view-byid');
    Route::get('admin/barang/create/form',[BarangController::class,'showFormCreateBarang'])->name('admin-create-barang-view');
    Route::post('admin/barang/create',[BarangController::class,'createBarang'])->name('admin-create-barang');
    Route::post('admin/barang/edit/{id}',[BarangController::class,'editBarang'])->name('admin-edit-barang');
    Route::delete('admin/barang/delete/{id}',[BarangController::class,'deleteBarang'])->name('admin-delete-barang');

    // Staff
    Route::get('admin/staff',[StaffController::class,'showStaff'])->name('admin-staff-view');
    Route::get('admin/staff/{id}',[StaffController::class,'showStaffByID'])->name('admin-staff-view-byid');
    Route::get('admin/staff/create/form',[StaffController::class,'showFormCreateStaff'])->name('admin-create-staff-view');

    Route::post('admin/staff/create',[StaffController::class,'createStaff'])->name('admin-create-staff');
    Route::post('admin/staff/edit/{id}',[StaffController::class,'editStaff'])->name('admin-edit-staff');
    Route::delete('admin/staff/delete/{id}',[StaffController::class,'deleteStaff'])->name('admin-delete-staff');

    // Customer
    Route::get('admin/customer',[CustomerController::class,'showCustomer'])->name('admin-customer-view');
    Route::get('admin/customer/{id}',[CustomerController::class,'showCustomerByID'])->name('admin-customer-view-byid');
    Route::get('admin/customer/create/form',[CustomerController::class,'showFormCreateCustomer'])->name('admin-create-customer-view');

    Route::post('admin/customer/create',[CustomerController::class,'createCustomer'])->name('admin-create-customer');
    Route::post('admin/customer/edit/{id}',[CustomerController::class,'editCustomer'])->name('admin-edit-customer');
    Route::delete('admin/customer/delete/{id}',[CustomerController::class,'deleteCustomer'])->name('admin-delete-customer');

     // Transaction 
    Route::get('admin/transaksi',[TransaksiController::class,'showTransaksi'])->name('admin-transaksi-view');

    // Laporan
    Route::get('admin/laporan',[LaporanController::class,'showLaporan'])->name('admin-laporan-view');
});


Route::middleware(['auth:staff'])->group(function () {
    Route::get('staff/dashboard',function() { 
        return view('dashboard');
    });

    // Barang
    Route::get('staff/barang',[BarangController::class,'showBarang'])->name('staff-barang-view');
    Route::get('staff/barang/{id}',[BarangController::class,'showBarangByID'])->name('staff-barang-view-byid');
    Route::get('staff/barang/create/form',[BarangController::class,'showFormCreateBarang'])->name('staff-create-barang-view');
    Route::post('staff/barang/create',[BarangController::class,'createBarang'])->name('staff-create-barang');
    Route::post('staff/barang/edit/{id}',[BarangController::class,'editBarang'])->name('staff-edit-barang');
    Route::delete('staff/barang/delete/{id}',[BarangController::class,'deleteBarang'])->name('staff-delete-barang');

    // Customer
    Route::get('staff/customer',[CustomerController::class,'showCustomer'])->name('staff-customer-view');
    Route::get('staff/customer/{id}',[CustomerController::class,'showCustomerByID'])->name('staff-customer-view-byid');
    Route::get('staff/customer/create/form',[CustomerController::class,'showFormCreateCustomer'])->name('staff-create-customer-view');

    Route::post('staff/customer/create',[CustomerController::class,'createCustomer'])->name('staff-create-customer');
    Route::post('staff/customer/edit/{id}',[CustomerController::class,'editCustomer'])->name('staff-edit-customer');
    Route::delete('staff/customer/delete/{id}',[CustomerController::class,'deleteCustomer'])->name('staff-delete-customer');

    // Transaction 
    Route::get('staff/transaksi',[TransaksiController::class,'showTransaksi'])->name('staff-transaksi-view');
    Route::put('staff/transaksi/confirm/{id}',[TransaksiController::class,'confirmTransaksi'])->name('staff-confirm-transaksi');

    // Laporan
    Route::get('staff/laporan',[LaporanController::class,'showLaporan'])->name('staff-laporan-view');
});

Route::middleware(['auth:customer'])->group(function () {
    Route::get('customer/dashboard',function() { 
        return view('dashboard');
    });

    // Barang
    Route::get('customer/barang',[BarangController::class,'showBarang'])->name('customer-barang-view');
    // Route::get('customer/barang/{id}',[BarangController::class,'showBarangByID'])->name('customer-barang-view-byid');

    // Transaction 
    Route::get('customer/transaksi',[TransaksiController::class,'showTransaksi'])->name('customer-transaksi-view');
    Route::post('customer/transaksi/create/{id}',[TransaksiController::class,'createTransaksi'])->name('customer-create-transaksi');
});



