@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>
                <div class="card-body">
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Barang</th>
                        <th scope="col">Customer</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksi as $no => $hasil)
                        <tr>
                        <th scope="row">{{ $no+1 }}</th>
                        <td>{{ $hasil->barangs->name }}</td>
                        <td>{{ $hasil->customers->name }}</td>
                        <td>{{ $hasil->amount }}</td>
                        <td>{{ $hasil->status }}</td>
                        <td>
                            @if (Auth::guard('staff')->check() && $hasil->status == 'pending')
                                    <form action="{{ route('staff-confirm-transaksi', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('put')
                                        <button class="btn btn-success btn-sm">Confirm</button>
                                    </form>
                            @endif 
                        </td>
                        </tr>
                        @endforeach
                        
                       
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
