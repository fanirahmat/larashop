@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>
                <div class="card-body">
                    @if (Auth::guard('admin')->check())
                        <a href="{{ route('admin-create-barang-view') }}" class="btn btn-success btn-sm">Add</a>
                    @elseif(Auth::guard('staff')->check())
                        <a href="{{ route('staff-create-barang-view') }}" class="btn btn-success btn-sm">Add</a>
                    @elseif(Auth::guard('customer')->check())
                    @endif

                    
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Desc</th>
                        <th scope="col">Type</th>
                        <th scope="col">Selling Price</th>
                        <th scope="col">Purchase Price</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barang as $no => $hasil)
                        <tr>
                        <th scope="row">{{ $no+1 }}</th>
                        <td>{{ $hasil->name }}</td>
                        <td>{{ $hasil->desc }}</td>
                        <td>{{ $hasil->type }}</td>
                        <td>{{ $hasil->selling_price }}</td>
                        <td>{{ $hasil->purchase_price }}</td>
                        <td>{{ $hasil->stock }}</td>
                        <td>
                            @if (Auth::guard('admin')->check())
                                    <a href="{{ route('admin-barang-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('admin-delete-barang', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                            @elseif(Auth::guard('staff')->check())
                                    <a href="{{ route('staff-barang-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('staff-delete-barang', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                            @elseif(Auth::guard('customer')->check())
                                    <form action="{{ route('customer-create-transaksi', $hasil->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-info btn-sm">Buy</button>
                                    </form>
                                    {{-- <a href="{{ route('customer-barang-view-byid', $hasil->id) }}" class="btn btn-info btn-sm">Buy</a> --}}
                            @endif
                                    
                                   
                                    
                        </td>
                        </tr>
                        @endforeach
                        
                       
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
