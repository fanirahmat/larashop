@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Barang</div>

                <div class="card-body">
                    @if (Auth::guard('admin')->check())
                        @isset($barang)
                            <form method="POST" action="{{ route('admin-edit-barang', $barang->id) }}" enctype="multipart/form-data">
                        @else
                            <form method="POST" action="{{ route('admin-create-barang') }}" enctype="multipart/form-data">
                        @endisset
                    @elseif(Auth::guard('staff')->check())
                        @isset($barang)
                            <form method="POST" action="{{ route('staff-edit-barang', $barang->id) }}" enctype="multipart/form-data">
                        @else
                            <form method="POST" action="{{ route('staff-create-barang') }}" enctype="multipart/form-data">
                        @endisset
                    @endif
                    
                     
                        @csrf
                        {{-- Name --}}
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Name</label>

                            <div class="col-md-6">
                                <input value="{{ $barang->name ?? "" }}" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Desc --}}
                        <div class="row mb-3">
                            <label for="desc" class="col-md-4 col-form-label text-md-end">Description</label>

                            <div class="col-md-6">
                                <input value="{{ $barang->desc ?? "" }}" id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" required autocomplete="desc" autofocus>

                                @error('desc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Type --}}
                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">Type</label>

                            <div class="col-md-6">
                                <input value="{{ $barang->type ?? "" }}" id="type" type="text" class="form-control @error('type') is-invalid @enderror" name="type" required autocomplete="type" autofocus>

                                @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Stock --}}
                        <div class="row mb-3">
                            <label for="stock" class="col-md-4 col-form-label text-md-end">Stock</label>

                            <div class="col-md-6">
                                <input value="{{ $barang->stock ?? 0 }}" id="stock" type="number" class="form-control @error('stock') is-invalid @enderror" name="stock" required autocomplete="stock">

                                @error('stock')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Selling Price --}}
                        <div class="row mb-3">
                            <label for="selling_price" class="col-md-4 col-form-label text-md-end">Selling Price</label>

                            <div class="col-md-6">
                                <input value="{{ $barang->selling_price ?? 0 }}" id="selling_price" type="number" class="form-control @error('selling_price') is-invalid @enderror" name="selling_price" required autocomplete="selling_price">

                                @error('selling_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Purchase Price --}}
                        <div class="row mb-3">
                            <label for="purchase_price" class="col-md-4 col-form-label text-md-end">Purchase Price</label>

                            <div class="col-md-6">
                                <input value="{{ $barang->purchase_price ?? 0 }}" id="purchase_price" type="number" class="form-control @error('purchase_price') is-invalid @enderror" name="purchase_price" required autocomplete="purchase_price">

                                @error('purchase_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- Image --}}
                        <div class="row mb-3">
                            <label for="image" class="col-md-4 col-form-label text-md-end">Image</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" required autocomplete="image" autofocus>

                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
