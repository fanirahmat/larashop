@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>
                <div class="card-body">
                    {{-- <a href="{{ route('admin-create-staff-view') }}" class="btn btn-success btn-sm">Add</a> --}}
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Barang</th>
                        <th scope="col">Selling Price</th>
                        <th scope="col">Purchase Price</th>
                        <th scope="col">Amount Sold</th>
                        <th scope="col">Gross Profit</th>
                        <th scope="col">Net Profit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($laporan as $no => $hasil)
                        <tr>
                            <th scope="row">{{ $no+1 }}</th>
                            <td>{{ $hasil->barangs->name }}</td>
                            <td>{{ $hasil->barangs->selling_price }}</td>
                            <td>{{ $hasil->barangs->purchase_price }}</td>
                            <td>{{ $hasil->amount_sold }}</td>
                            <td>{{ $hasil->gross_profit }}</td>
                            <td>{{ $hasil->net_profit }}</td>
                        </tr>
                        @endforeach
                        
                        
                        
                        
                        
                       
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
