@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>
                <div class="card-body">
                    <a href="{{ route('admin-create-staff-view') }}" class="btn btn-success btn-sm">Add</a>
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($staff as $no => $hasil)
                        <tr>
                        <th scope="row">{{ $no+1 }}</th>
                        <td>{{ $hasil->name }}</td>
                        <td>{{ $hasil->gender }}</td>
                        <td>{{ $hasil->username }}</td>
                        <td>{{ $hasil->email }}</td>
                        <td>
                            <a href="{{ route('admin-staff-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('admin-delete-staff', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                   
                                    
                        </td>
                        </tr>
                        @endforeach
                        
                       
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
