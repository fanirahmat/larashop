@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $title ?? "" }}</div>

                <div class="card-body">
                    @if (Auth::guard('admin')->check())
                        <a href="{{ route('admin-create-customer-view') }}" class="btn btn-success btn-sm">Add</a>
                    @elseif(Auth::guard('staff')->check())
                        <a href="{{ route('staff-create-customer-view') }}" class="btn btn-success btn-sm">Add</a>
                    @elseif(Auth::guard('customer')->check())
                    @endif

                     <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Address</th>
                        <th scope="col">ID Card Image</th>
                        <th scope="col">Date of Birth</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customer as $no => $hasil)
                        <tr>
                        <th scope="row">{{ $no+1 }}</th>
                        <td>{{ $hasil->name }}</td>
                        <td>{{ $hasil->gender }}</td>
                        <td>{{ $hasil->address }}</td>
                        <td>{{ $hasil->id_card_image }}</td>
                        <td>{{ $hasil->dob }}</td>
                        <td>{{ $hasil->username }}</td>
                        <td>{{ $hasil->email }}</td>
                        <td>
                            @if (Auth::guard('admin')->check())
                                    <a href="{{ route('admin-customer-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('admin-delete-customer', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                            @elseif(Auth::guard('staff')->check())
                                    <a href="{{ route('staff-customer-view-byid', $hasil->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('staff-delete-customer', $hasil->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                            @elseif(Auth::guard('customer')->check())
                            @endif
                                    
                                   
                                    
                        </td>
                        </tr>
                        @endforeach
                        
                       
                    </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection